hi(byte) Quite often 8-bit variables are required, usually to access memory
locations. Traditionally the tt(char) type has been
used for that, but tt(char) is a signed type and when inserting a tt(char)
variable into a stream the character's representation instead of its value is
used. Maybe more important is the inherent confusion when using tt(char) type
variables when only using its (unsigned) value: a tt(char) documents to the
reader that text is used instead of mere 8-bit values, as used by the smallest
addressable memory locations.

Different from the tt(char) type the tt(std::byte) type intends to merely
represent an 8-bit value. In order to use tt(std::byte) the tthi(cstddef)
header file must be included.

The tt(byte) is defined as a strongly typed enum, simply embedding an
tt(unsigned char):
    verb(    enum class byte: unsigned char
    {};)
    As a tt(byte) is an enum without predefined enum values plain assignments
can only be used between tt(byte) values. tt(Byte) variables can be
initialized using curly braces around an existing tt(byte) or around fixed
values of at most 8 bits (see #1 in the following example). If the specified
value doesn't fit in 8 bits (#2) or if the specified value is neither a
tt(byte) nor an tt(unsigned char) type variable (#3) the compiler reports an
error.

Assignments or assignment-like initializations using rvalues which are
tt(bytes) initialized using parentheses with values not fitting in 8 bits are
accepted (#4, #5). In these cases, the specified values are truncated to their
lowest 8 bits. Here are the illustrations:
    verbinsert(-s4 //init examples/byte.cc)

The tt(byte) type supports all bit-wise operations, but the right-hand operand
of the bit-wise operator must also be a tt(byte). E.g.,
    verb(       value &= byte(0xf0);)
 tt(Byte) type values can also be ordered and compared for (in)equality.

Unfortunately, no other operations are supported. E.g., tt(bytes) cannot be
added and cannot be inserted into or extracted from streams, which somehow
renders the tt(std::byte) less useful than ordinary types (like tt(unsigned
int, uint16_t)). When needed such operations em(can) be supported using casts
(covered in section ref(CPPCASTS)), but it's considered good practice to avoid
casts whenever possible. However, bf(C++) allows us to define a byte-type that
em(does) behave like an ordinary numeric type, including and extracting its
values into and from streams. In section ref(BYTE) such a type is developed.

