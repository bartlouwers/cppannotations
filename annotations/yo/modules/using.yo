When using modules, the using code does not specify in which module a
component is declared. If a function like tt(square) is also declared
elsewhere and both declarations are available to a using function then that
generates an ambiguity-error.

    hi(module: compilation)hi(module: -fmodules-ts)
    hi(-fmodules-ts: module compiler flag)
Assuming tt(complete.cc) declares module tt(complete) then the module
interface file can be compiled using the following command:
        verb(    g++ -c --std=c++20 -fmodules-ts -Wall complete.cc)
    resultin in the following file system elements:
    itemization(
    itt(complete.o,) the compiled module
    itt(gcm.cache/complete.gcm,) the module's interface.nl()
        the interface resembles a precompiled header, but usually it is much
        smaller.
    )

Once the module interface file has been compiled the remaining source files of
the module can be compiled. Source files using module components must 
    hi(module: import)
    em(import) it. Once imported, a source file has access to all the module's
exported components. In the following example tt(main) uses components of
module tt(complete):
    verbinsert(-ans4 examples/complete/main.cc)
    itemization(
    it() At line 2 the module's components are made available through the
        tt(import complete;) specification. Note the semicolon ending the
        specification.

        As tt(main) calls tt(std::cout, iostream) must be included. No
        tt(using namespace std) was declared, but it could have been. There's
        no required ordering of tt(import) and tt(include) lines: tt(import
        complete;), now after tt(#include <iostream>) might as well have been
        specified first;
    
    it() At line 3 a namespace declaration is used, directly making the
        module's components defined in the tt(namespace FBB) available;

    it() At line 7, 8, and 10 components of tt(complete) are used. Note that
        no additional specification is required indicating that these are
        module components. Just using their names is all it takes.
    )

When compiling tt(main.cc) the tt(gcm.cache/complete.gcm) file must be
available (but see also section ref(MODMAPPER)). Consequently tt(complete.cc)
must be compiled before tt(main.cc). As tt(main.cc)
imports a module the flag tt(-fmodules-ts) must also be specified when
compiling tt(main.cc).

As an aside: header files can also be imported instead of being included. This
is covered in section ref(HDRIMPORT).

Modules, like classes, can be used by other modules as well. When modules use
modules a hi(module: hierarchy) hierarchical ordering results, which must be
acknowledged when compiling the modules (cf. section ref(MODMAPPER)): the
least dependent module interface must be compiled first, the most dependent
module interface must be compiled last.
