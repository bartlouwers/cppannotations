Modules are generally designed using source files rather than header
files. Consider the following suggestions when designing modules:
    itemization(
    it() A module interface source file is located in a directory having the
        module's name. Module interface files should preferably only contain
        declarations, while other source files in the module's directory
        contain the implementations of the module's components.

        The module interface file has the same filename as the module's name,
        and has a standard bf(C++) extension, like tt(.cc);

    it() Data declared by modules are defined in a tt(data.cc) file, and
        free functions declared by modules are defined in
        tt(functionname.cc), where tt(functionname) is the name of the
        module's free function;

    it() Members of classes declared by modules are defined in subdirectories
        of the module's directory, where each subdirectory is named after its
        class. By convention either all of the subdirectories of the module's
        directory should contain sources implementing such members or a
        separate tt(CLASSES) file is used, specifying which subdirectories to
        visit;

    it() Classes can still be defined outside modules: such classes should be
        implemented as before, where each class is defined in its own
        subdirectory.
    )

    hi(module: interface unit)
    Here's an example of a module interface:
    verbinclude(-ans4 examples/complete/complete/complete.cc)

COMMENT(
// modules cannot be embedded in namespaces, but namespaces can be
// embedded in modules. Also, the module.gcm cache files are directly
// connected to the module.cc source file, and cannot be renamed.
// The module name can freely be chosen, but somehow collisions of filenames
// defining modules must be prevented.
END)

This example illustrates several characteristics of modules:
    itemization(
    it() Line 1: hi(module: export) module interfaces start with tt(export
        module) followed by the module's name. Names are identifiers, possibly
        preceded by series of `tt(identifier .)' or `tt(identifier :)'
        sequences. The former syntax merely defines a module's name (like
        tt(this.is.my.module)), the latter is used for em(partitions), covered
        in section ref(PARTITIONS). 

        Because module interfaces em(must) begin with an tt(export module
        'module-name') line, module interface files cannot define multiple
        modules; 

    it() At line 5 tt(counter) is declared as an variable in a namespace:
        modules can specify that their components reside in namespaces, but
        the reverse is not possible: namespaces cannot contain module
        definitions. 

        Variable tt(counter's) declaration starts with the keyword
        tt(export). Module components are by default, like private members of
        classes, only available to other module components. To make them
        available outside of their module their declarations must specify
        tt(export). In the context of modules tt(export) acts like tt(public)
        in classes.

        Because of tt(extern) the variable tt(counter) is only declared, and
        it is defined elsewhere (in tt(data.cc), see below). If tt(extern) is
        omitted then the variable is defined in the interface, which is
        possible, but not advised as its breaks the convention that
        interfaces/declarations should be separated from definitions;

    it() At line 8 tt(lastValue) is declared. Its declaration doesn't specify
        tt(export), and thus tt(lastValue) is a only available to components
        of the module tt(complete);

    it() At line 10, the function tt(square) is declared as a free function,
        also using tt(export), so it is available outside of the module;

    it() Finally, the class tt(Class) (line 12) is declared, also specifying
        tt(export): tt(Class) objects can be defined and used outside of the
        module. 
    )

