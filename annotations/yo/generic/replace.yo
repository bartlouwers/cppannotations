hi(replace) hi(replace_copy) hi(replace_copy_if) hi(replace_if)
    itemization(
        it() Header file: tt(<algorithm>)
        it() Function prototype:
        itemization(
            itt(void replace([ExecPol,] ForwardIterator first, 
    ForwardIterator last, Type const &oldvalue, Type const &newvalue);)
            itt(ForwardIterator replace_if([ExecPol,] ForwardIterator first,
    ForwardIterator last, UnaryPredicate pred, Type const &value);)
            itt(OutputIterator replace_copy([ExecPol,] InputIterator first, 
InputIterator last, OutputIterator result, Type const &oldvalue, 
Type const &newvalue);)
            itt(OutputIterator replace_copy_if([ExecPol,] 
ForwardIterator first, ForwardIterator last, OutputIterator result, 
UnaryPredicate pred, Type const &value);)
        )
        it() Description:
        itemization(
            it() The first predicate: all elements equal to tt(oldvalue) in
the range pointed to by rangett(first, last) are replaced by a copy of
tt(newvalue). The algorithm uses tt(operator==) of the data type to which the
iterators point.
            it() The second prototype: elements in the range pointed to by
rangett(first, last) for which the unary predicate tt(pred) evaluates as
tt(true) are replaced by tt(value).
            it() The third prototype: all elements equal to tt(oldvalue) in
the range pointed to by rangett(first, last) are sent to the tt(result) output
iterator, replacing elements equal to tt(oldValue) by tt(newValue), using
tt(operator==) of the data type to which the iterators point.
            it() The fourth prototype: the elements in the range pointed to by
rangett(first, last) for which the unary predicate tt(pred) returns tt(false)
are sent to the tt(result) output iterator, and tt(value) is sent to
tt(result) if tt(pred) returns tt(true). The range rangett(first, last) is not
modified.
        )
        it() Example:
        verbinclude(-a examples/replace.cc)
    )
