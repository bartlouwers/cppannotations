    #include <algorithm>
    #include <iostream>
    #include <string>
    #include <functional>
    #include <iterator>
    using namespace std;

    int main()
    {
        string words[] =
            { "kilo", "alpha", "lima", "mike", "alpha", "november", "alpha",
                "oscar", "alpha", "alpha", "papa", "quebec" };

//        size_t const size = sizeof(words) / sizeof(string);
//        string remaining[size];
//                            size -
//                            count_if
//                            (
//                                words, words + size,
//                                bind2nd(equal_to<string>(), "alpha")
//                            )
//                        ];

        vector<string> remaining;
//        remaining.resize(size(words));

//        string *returnvalue =
//                    auto ret =
                    remove_copy_if
                    (
                        words, end(words),
                        //remaining.begin(),
                        back_insert_iterator<vector<string>>(remaining),
                            [&](string const &str)
                            {
                                return str == "alpha";
                            }
                    );

        cout << "Removing all \"alpha\"s:\n";
        copy(remaining.begin(),
                    remaining.end(),
                    //ret,
                ostream_iterator<string>(cout, " "));
        cout << '\n';
    }
    /*
        Displays:
            Removing all "alpha"s:
            kilo lima mike november oscar papa quebec
    */
