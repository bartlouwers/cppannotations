#include <iostream>
#include <filesystem>
#include <chrono>

using namespace std;
using namespace chrono;
using namespace filesystem;

int main()
{
    //system_clock::to_sys(system_clock::now());    // not for system_clock
    // steady_clock::to_sys(steady_clock::now());   // not for steady_clock
    // high_resolution_clock::to_sys(               // not for high_res._clock
    //              high_resolution_clock::now());
    __file_clock::to_sys(__file_clock::now());
    //__file_clock::to_time_t(__file_clock::now()); // not in __file_clock
    // steady_clock::to_time_t(steady_clock::now());// not in steady_clock
}
