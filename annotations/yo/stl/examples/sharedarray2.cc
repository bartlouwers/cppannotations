#include <memory>
#include <iostream>

using namespace std;

//SHAREDARRAY
    struct Type
    {
        int value = 0;

        ~Type()
        {
            cout << "destr " << this << '\n';
        }
    };

    template<typename Tp>
    struct Deleter
    {
        size_t d_size;
        Deleter(size_t size)
        :
            d_size(size)
        {}
        void operator()(Tp **ptr)
        {
            for (; d_size--; )
                delete ptr[d_size];

            delete[] ptr;           // <-- NOTE !
        }
    };

    int main()
    {
        shared_ptr<Type *[]> sp{ new Type *[2] {new Type, new Type},
                                 Deleter<Type>{ 2 } };
    }
