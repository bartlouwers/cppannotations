The class tt(std::weak_ordering)hi(weak_ordering) is used when
implementing the spaceship operator for classes that support all comparison
operators (where one operand may be zero), and that do not support
substitutability.

The class tt(weak_ordering) differs from the tt(partial_ordering) class in
that em(unordered) cannot be used as a comparison result. Like the class
tt(partial_ordering) it provides free functions for all comparison
operations (tt(==, !=, <, <=, >,) and tt(>=)) expecting tt(partial_ordering)
arguments (one argument may be 0). It also defines three static objects which
can be returned by the spaceship operator:
    itemization(
    itt(weak_ordering::less), returned when the lhs operand of the
        spaceship operator should be ordered before the rhs operand;
    itt(weak_ordering::equal), indicating equality: there is no
        ordering preference between the two operands of the spaceship operator;
    itt(weak_ordering::greater), returned when the lhs operand of the
        spaceship operator should be ordered after the rhs operand;
    )

The example in the previous section can easily adapted to the
tt(weak_ordering) comparison class: if the tt(roadTax) members of vehicles for
which no road tax is due return zero then tt(RoadTax's) spaceship operator can
be implemented this way:
    verb(    weak_ordering RoadTax::operator<=>(Vehicle const &lhs, 
                                          Vehicle const &rhs)
    {
        return
          lhs.roadTax() < rhs.roadTax() ? weak_ordering::less      :
          lhs.roadTax() > rhs.roadTax() ? weak_ordering::greater   :
                                          weak_ordering::equal;
    })



