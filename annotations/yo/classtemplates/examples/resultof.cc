#include <iostream>
#include <string>
#include <functional>

using namespace std;

template <typename Class>
class Filter
{
    Class obj;
    public:
        template <typename Arg>
        typename std::result_of<Class(Arg)>::type
            operator()(Arg const &arg) const
            {
                return obj(arg);
            }
};


struct Convert      // if there's just one ReturnType operator()(...), simply
{                   // specify   `using type = ReturnType'
    template <typename Signature>
    struct result;

    double operator()(int x) const
    {
        return 12.5;
    }
    template <typename Class>
    struct result<Class(int)>
    {
        using type = double;
    };

    string operator()(double x) const
    {
        return "hello world"s;
    }
    template <typename Class>
    struct result<Class(double)>
    {
        using  type = string;
    };
};

int main()
{
    Filter<Convert> fc;

    cout << fc(5.5).length() << '\n';
    cout << fc(5) << '\n';
}
