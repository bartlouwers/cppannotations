#ifndef INCLUDED_DONE_
#define INCLUDED_DONE_

#include "../../promisebase/promisebase.h"

class Done
{
    struct State: public PromiseBase<Done, State>
    {};

    std::coroutine_handle<State> d_handle;
    static size_t s_count;

    public:
        using promise_type = State;
        using Handle = std::coroutine_handle<State>;

        explicit Done(Handle handle);
        ~Done();

        Handle handle() const;
};

inline Done::Handle Done::handle() const
{
    return d_handle;
}

extern Done g_done;

#endif
