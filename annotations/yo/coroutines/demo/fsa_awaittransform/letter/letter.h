#ifndef INCLUDED_LETTER_
#define INCLUDED_LETTER_

#include "../../promisebase/promisebase.h"
#include "../awaiter/awaiter.h"

class Letter
{
    struct State: public PromiseBase<Letter, State>
    {
        template <typename Handler>
        Awaiter await_transform(Next<Handler> &&next);
    };

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;
        using Handle = std::coroutine_handle<State>;

        explicit Letter(Handle handle);
        ~Letter();

        Handle handle() const;
};

template <typename Handler>
Awaiter Letter::State::await_transform(Next<Handler> &&next)
{
    return Awaiter{ next.d_handle };
}

inline Letter::Handle Letter::handle() const
{
    return d_handle;
}

extern Letter g_letter;

#endif
