#ifndef INCLUDED_DIGIT_
#define INCLUDED_DIGIT_

#include "../../promisebase/promisebase.h"

class Digit
{
    struct State: public PromiseBase<Digit, State>
    {};

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;
        using Handle = std::coroutine_handle<State>;

        explicit Digit(Handle handle);
        ~Digit();

        Handle handle() const;
};

inline Digit::Handle Digit::handle() const
{
    return d_handle;
}


extern Digit g_digit;

#endif
