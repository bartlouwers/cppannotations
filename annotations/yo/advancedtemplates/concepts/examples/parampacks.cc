#include <iostream>
#include <concepts>
#include <utility>
#include <string>
#include <vector>
#include <queue>

//sum
size_t sum()
{
    return 0;
}

template <typename First, typename  ...Types>
size_t sum(First &&first, Types &&...types)
{
    return first.size() + sum(std::forward<Types>(types)...);
}
//=


//hassize
template <typename Types>
concept HasSize =
    requires (Types type)
    {
        { type.size() } -> std::same_as<size_t>;
    };
//=

//sum2
size_t sum2()
{
    return 0;
}

template <HasSize First, HasSize  ...Types>
size_t sum2(First &&first, Types &&...types)
{
    return first.size() + sum2(std::forward<Types>(types)...);
}
//=

//sumfun
template <HasSize ...Types>
void fun(Types &&...obj)
{
    sum(std::forward<Types &&>(obj)...);
}
//=

//sizetypes
template <HasSize ...Types>
class SizeTypes
{
    // ...
};
//=

using namespace std;

//main
int main()
{
    fun(queue<int>{}, vector<int>{}, string{});
    cout << sum2(queue<int>{}, vector<int>{}, string{}) << '\n';
}
//=
